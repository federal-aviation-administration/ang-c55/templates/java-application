import os
import platform
import subprocess
import shutil

gitenv = shutil.which('git')

if gitenv is None:
    print ('ERROR: git requested, but git binary not found')
    exit(1)

subprocess.run([gitenv, "init", "--initial-branch=main"])
subprocess.run([gitenv, "add", "."])
subprocess.run([gitenv, "update-index", "--chmod=+x", "gradlew"])
subprocess.run([gitenv, "commit", "-m", "'Initial project setup'"])

gradle_command = "gradlew.bat" if platform.system() == "Windows" else "./gradlew"
    
result = subprocess.run([gradle_command, "build", "javadoc"])

if result.returncode != 0:
    print('ERROR: gradle build seems to be broken')
    exit(1)
