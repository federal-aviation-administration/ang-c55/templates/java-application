# {{cookiecutter.__project_name_pascal_case}}


[![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-lightgrey.png)](http://creativecommons.org/publicdomain/zero/1.0/) 


## License

### Creative Commons Zero v1.0 Universal

This is a work created by or on behalf of the United States Government. To the extent that this work is not already in 
the public domain by virtue of 17 USC § 105, the FAA waives copyright and neighboring rights in the work worldwide 
through the CC0 1.0 Universal Public Domain Dedication (which can be found at https://creativecommons.org/publicdomain/zero/1.0/).

See [LICENSE.txt](LICENSE.txt) and [NOTICE.txt](NOTICE.txt) in the root of this project for the full terms of the license.


### Third Party

This project utilizes 3rd party libraries that are distributed under their own terms. For a complete list of libraries 
and coordinating licenses see [LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt) in the root of this project.
