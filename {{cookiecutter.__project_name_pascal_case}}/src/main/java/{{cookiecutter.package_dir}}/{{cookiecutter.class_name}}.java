/*
 * {{cookiecutter.__project_name_pascal_case}}
 *
 * This is a work created by or on behalf of the United States Government. To the
 * extent that this work is not already in the public domain by virtue of
 * 17 USC § 105, the FAA waives copyright and neighboring rights in the work
 * worldwide through the CC0 1.0 Universal Public Domain Dedication (which can be
 * found at https://creativecommons.org/publicdomain/zero/1.0/).
 */

package {{cookiecutter.package}};

/**
 * The top-level class of this application.
 * 
 * @author US DOT, FAA, Office of NextGen, Modeling and Simulation Branch
 */
public class {{cookiecutter.class_name}} {

	private {{cookiecutter.class_name}}() {/* NO-OP, do not allow instantiation by default. */}

	/**
	 * The main entry point into this application.
	 * 
	 * @param args  The list of arguments to this application.
	 */
	public static void main(String[] args) {

	}

}
